<?php


class Printer
{
    public function do () : void
    {
        for ($i = 1;$i < 101;$i++)
        {
            print ("\nPrinting number : " . $this->getNumber($i));
        }
    }
    public function getNumber(int $num): string
    {
        $isDivisorOf3 = $num % 3 == 0;
        $isDivisorOf5 = $num % 5 == 0;

        $result = strval($num);

        while ($isDivisorOf5 & !$isDivisorOf3)
        {
            $result = "IT";
            break;
        }

        while ($isDivisorOf3 & !$isDivisorOf5)
        {
            $result = "Linio";
            break;
        }

        while ($isDivisorOf3 & $isDivisorOf5)
        {
            $result = "Linianos";
            break;
        }
        return $result;

    }

}


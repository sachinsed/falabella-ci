<?php

class PrinterTest extends PHPUnit_Framework_TestCase
{

    public function test_should_get_1_for_1()
    {
        $expected = "1";

        $this->assertSame($expected, $this->getObject()->getNumber(1));
    }

    public function test_should_get_Linio_for_3()
    {
        $expected = "Linio";

        $this->assertSame($expected, $this->getObject()->getNumber(3));
    }

    public function test_should_get_IT_for_5()
    {
        $expected = "IT";

        $this->assertSame($expected, $this->getObject()->getNumber(5));
    }

    public function test_should_get_Linianos_for_5()
    {
        $expected = "Linianos";

        $this->assertSame($expected, $this->getObject()->getNumber(15));
    }

    public function getObject(): Printer
    {
        return new Printer();
    }
}

